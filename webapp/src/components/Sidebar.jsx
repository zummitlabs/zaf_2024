import React, { useRef, useState } from 'react';
import zlogo from '../assets/zlogo.png';
import home from '../assets/icons/home.png';
import modelIcon from '../assets/icons/modelIcon.png';
import membersIcon from '../assets/icons/membersIcon.png';
import frame from '../assets/icons/frame.png';
import logout from '../assets/icons/logout.png';
import { models, members } from '../data';

/* Fixed */
import vector from '../assets/Vector.png';
import { NavLink } from 'react-router-dom';
import dropdownIcon from '../assets/icons/dropdownIcon.png';
import '../App.css';

const Sidebar = () => {
  const menu1 = [
    {
      id: 1,
      name: 'Home',
      icon: home,
      dropdown: null,
      route: '/'
    },
    {
      id: 2,
      name: 'Our Models',
      icon: modelIcon,
      dropdown: models,
      active: false
    }
  ];

  const menu2 = [
    {
      id: 1,
      name: 'Members',
      icon: membersIcon,
      dropdown: members,
      active: false

    },
    {
      id: 2,
      name: 'Profile',
      icon: frame,
      dropdown: null,
      route: '/profile'
    },
    {
      id: 3,
      name: 'Log Out',
      icon: logout,
      dropdown: null,
      logout: true,
      route: '/logout'
    }
  ]

  const SidebarItem = ({ item }) => {
    const arrow = useRef();
    const [active, setActive] = useState(item.active ? item.active : null);

    return (
      <>
        {!item.dropdown ? (
          <NavLink to={item.route} className={({ isActive }) => (isActive ? 'sidebaritem-main active' : 'sidebaritem-main')} >
            <div className='sidebaritem-container'>
              <img className='sidebaritem-icon' src={item.icon} alt={item.icon} />
              <p style={item.logout ? { color: '#FF4040' } : { color: '#FFFFFF' }}>{item.name}</p>
            </div>
          </NavLink>) : (
          <>

            <div className={active ? 'sidebaritem-main active' : 'sidebaritem-main'} onClick={() => setActive((prev) => !prev)} >
              <div className='sidebaritem-container'>
                <img className='sidebaritem-icon' src={item.icon} alt={item.icon} />
                <p style={item.logout ? { color: '#FF4040' } : { color: '#FFFFFF' }}>{item.name}</p>
                {item.dropdown ? <img className={active ? 'arrow arrow-up' : 'arrow arrow-down'} src={vector} alt='arrow' ref={arrow} /> : <p ref={arrow}></p>}
              </div>
            </div>

            {active ? <Dropdown menu={item.dropdown} /> : null}

          </>
        )}
      </>
    )
  }

  const Dropdown = ({ menu }) => {

    return (
      <div className='dropdown-main'>
        {menu.map((element) => (
          <DropdownItem key={element.id} name={element.name} route={element.route} />
        ))}
      </div>
    )
  }

  const DropdownItem = ({ name, route }) => {
    return (
      <div className='dropdown-item-main'>
        <img src={dropdownIcon} alt='dropdown-left-icon' />
        <NavLink to={route} className={({ isActive }) => (isActive ? 'dropdown-item-container dropdown-item-active' : 'dropdown-item-container')}>
          <p>{name}</p>
        </NavLink>
      </div>
    )
  }

  return (
    <div className='sidebar-main w-[20%] h-full fixed overflow-auto flex flex-col gap-8 items-center font-outfit bg-[#000617] py-6'>
      <img className='mt-10 w-[80%]' src={zlogo} alt="zummit-logo" />
      <div className='flex flex-col gap-40'>
        <div className='flex flex-col gap-8'>
          {menu1.map((item) => (
            <SidebarItem key={item.id} item={item} />
          ))}
        </div>
        <div className='flex flex-col gap-8'>
          {menu2.map((item) => (
            <SidebarItem key={item.id} item={item} />
          ))}
        </div>
      </div>
    </div>
  )
}

export default Sidebar