import React, { useRef, useState } from 'react';
import clientImage from '../assets/carouselImages/clientImage.jpg';
import starIcon from '../assets/icons/starIcon.png';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Testimonials = () => {
    const clients = [
        {
            id: 1,
            name: 'Amy Watson',
            stars: 5,
            img: clientImage,
            company: 'Skylark Corp.',
            title: 'Co-Founder & CEO',
            review: 'Zummit Infolabs excels in delivering cutting-edge AI tools with user-friendly interfaces. Their efficient solutions for tasks like resume screening, emotion detection, and background removal significantly enhance productivity and user experience. Their innovative approach and excellent design make them highly recommended!'
        },
        {
            id: 2,
            name: 'Amy Watson',
            stars: 4,
            img: clientImage,
            company: 'Skylark Corp.',
            title: 'Co-Founder & CEO',
            review: 'Zummit Infolabs excels in delivering cutting-edge AI tools with user-friendly interfaces. Their efficient solutions for tasks like resume screening, emotion detection, and background removal significantly enhance productivity and user experience. Their innovative approach and excellent design make them highly recommended!'
        },
        {
            id: 3,
            name: 'Amy Watson',
            stars: 5,
            img: clientImage,
            company: 'Skylark Corp.',
            title: 'Co-Founder & CEO',
            review: 'Zummit Infolabs excels in delivering cutting-edge AI tools with user-friendly interfaces. Their efficient solutions for tasks like resume screening, emotion detection, and background removal significantly enhance productivity and user experience. Their innovative approach and excellent design make them highly recommended!'
        },
        {
            id: 4,
            name: 'Amy Watson',
            stars: 4,
            img: clientImage,
            company: 'Skylark Corp.',
            title: 'Co-Founder & CEO',
            review: 'Zummit Infolabs excels in delivering cutting-edge AI tools with user-friendly interfaces. Their efficient solutions for tasks like resume screening, emotion detection, and background removal significantly enhance productivity and user experience. Their innovative approach and excellent design make them highly recommended!'
        },
        {
            id: 5,
            name: 'Amy Watson',
            stars: 5,
            img: clientImage,
            company: 'Skylark Corp.',
            title: 'Co-Founder & CEO',
            review: 'Zummit Infolabs excels in delivering cutting-edge AI tools with user-friendly interfaces. Their efficient solutions for tasks like resume screening, emotion detection, and background removal significantly enhance productivity and user experience. Their innovative approach and excellent design make them highly recommended!'
        },

    ]
    const card = useRef();

    const [oldSlide, setOldSlide] = useState(0);
    const [activeSlide, setActiveSlide] = useState(0);
    const [activeSlide2, setActiveSlide2] = useState(0);

    const settings = {
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: clients.length % 3,
        slidesToScroll: 1,
        beforeChange: (current, next) => {
            setOldSlide(current);
            setActiveSlide(next);
        },
        afterChange: current => setActiveSlide2(current)
    }

    return (
        <div className='testimonials-main my-4'>
            <h2 className='text-3xl font-medium my-4'>Hear it from our clients</h2>
            <div className='w-full'>
                <Slider className='testimonials-container' {...settings}>
                    {clients.map((client) => (
                        <div className={activeSlide === client.id - 1 ? 'client-main' : 'client-blur'} key={client.id} ref={card}>
                            <div className='client-container'>
                                <img className='client-image' src={client.img} alt='client-image' />
                                <div className='client-data'>
                                    <p id='clientCompany'>{client.company}</p>
                                    <h3 id='clientName'>{client.name}</h3>
                                    <p id='clientTitle'>{client.title}</p>
                                </div>
                                <div className='client-stars'>
                                    {new Array(client.stars).fill('').map((_, index) => (
                                        <img src={starIcon} alt="star-icon" />
                                    ))}
                                </div>
                            </div>
                            <div className='client-review'>{client.review}</div>
                        </div>
                    ))}
                </Slider>
            </div>
        </div>
    )
}

export default Testimonials