import React, { useState } from 'react';
import { models, tools } from '../data';
import Slider from 'react-slick';

/* Components Import */
import Sidebar from '../components/Sidebar';
import Testimonials from '../components/Testimonials';
import '../App.css';

/* Icons Import */
import zlogo from '../assets/zlogo.png'
import bannerImage from '../assets/bannerImage.png';
import chatboxLogo from '../assets/icons/chatboxLogo.png';
import search from '../assets/icons/search.png';
import emotionDetection from '../assets/icons/emotionDetection.png';
import timeAnalysis from '../assets/icons/timeAnalysis.png';
import reinforcement from '../assets/icons/reinforcement.png';
import language from '../assets/icons/language.png';
import analytics from '../assets/icons/analytics.png';
import anomaly from '../assets/icons/anomaly.png';
import generativeSol from '../assets/icons/generativeSol.png';
import machineLearning from '../assets/icons/machineLearning.png'
import ellipse from '../assets/icons/ellipse.png';
import checkIcon from '../assets/icons/checkIcon.png';
import { Link } from 'react-router-dom';

const Home = () => {
  const Chatbox = () => {
    const [active, setActive] = useState(false);
    return (
      <>
        {active
          ? (<div className='w-80 h-80 bg-white fixed right-[7rem] bottom-[1.87rem] rounded-full z-[9999]'></div>)
          : null
        }
        <div
          className='w-[76px] h-[76px] fixed right-[30px] bottom-[30px]'
          onClick={() => setActive((prev) => !prev)}
        >
          <img src={chatboxLogo} alt="chatbox-logo" />
        </div>
      </>
    )
  }

  const SearchBox = () => {
    return (
      <div className='w-[20%] h-12 flex items-center px-3 mt-1 gap-4 bg-[#ffffff1a] border-[0.5px] border-[#454343] rounded-xl text-[#9c9c9c]'>
        <img className='' src={search} alt="search-icon" />
        <input
          className=' border-none bg-[#ffffff00] text-[0.87rem] leading-[1.05rem] font-medium font-["Inter"] outline-none'
          type="text"
          placeholder='Search Zummit...'
        />
      </div>
    )
  }

  const BannerHome = () => {
    return (
      <div className='bannerhome-main w-[98%] flex flex-col items-center rounded-3xl border-[0.5px] border-[#454343] font-["Outfit"] p-5 gap-8 my-5'>
        <h2 className='text-3xl font-semibold text-white'>Zummit's AI Framework</h2>
        <p className='text-xl font-normal text-center text-[#c8c8c8]'>
          Welcome to Zummit AI Framework, powered by Zummit Infolabs- a suite of advanced AI tools designed
          to streamline your business operations and enhance productivity. Our state-of-the-art AI solutions is at the
          forefront of technological innovation, leveraging artificial intelligence to solve real-world problems.
        </p>
        <div className='flex justify-center gap-7'>
          <button
            className='px-8 py-4 rounded-xl text-white text-xl font-semibold cursor-pointer bg-[#4a5fa2] border-none'
            id="register-button"
          >
            Register Now
          </button>
          <button
            className='px-14 py-4 rounded-xl text-white text-xl font-semibold cursor-pointer bg-[#ffffff1a] border-2 border-[#4a5fa2]'
            id="signin-button"
          >
            Sign In
          </button>
        </div>
      </div>
    )
  }

  const CarouselElement = ({ data, type }) => {
    const settings = {
      arrows: false,
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 4.75,
      slidesToScroll: 3,
      dotsClass: `slick-dots`
    }
    return (
      <div className='bannerhome-main w-[98%] flex flex-col items-center border-[0.5px] border-[#454343] rounded-3xl font-outfit pb-14 my-4'>
        <h2 className='font-semibold text-3xl text-white py-5'>
          {type === 'models' ? 'Our Models' : 'Trending AI tools'}
        </h2>
        <Slider className='slick-parent' {...settings}>
          {data.map((element) => (
            <Link to={`${element.route}`}
              className='slick-slide flex flex-col items-center cursor-pointer bg-[#79797917] border-[0.5px] border-[#454343] rounded-3xl hover:bg-[#4a5fa2]'
              key={element.id}
            >
              <img className='w-full' src={element.img} />
              <div className='flex flex-col items-center gap-2 rounded-xl p-4'>
                <h3 className='font-semibold text-xl text-white'>{element.name}</h3>
                <p
                  className='font-medium text-[14px] leading-[17.64px] text-center text-[#b6b6b6] hover:text-white'
                  id={type === 'models' ? 'modP' : 'aiP'}
                >
                  {element.desc}
                </p>
              </div>
            </Link>
          ))}
        </Slider>
      </div>
    )
  }

  const SpecialArticle = () => {
    return (
      <div className='special-article-main w-[98%] flex flex-col items-center border-[0.5px] border-[#454343] rounded-3xl font-outfit p-4 my-4'
      >
        <h2 className='font-semibold text-3xl text-white my-8'>Where we shine</h2>
        <div className='special-article-grid'>
          {elements.map((element) => (
            <div className='flex flex-col items-center gap-3' key={element.id}>
              <div className='flex justify-center items-center relative'>
                <img src={ellipse} alt='ellipse' />
                <img className='absolute' id="img2" src={element.img} alt={element.img} />
              </div>
              <h3 className='text-white font-semibold text-xl'>{element.name}</h3>
              <p className='w-[90%] font-["Inter"] text-xs font-normal text-center text-[#cdcdcd]'>{element.desc}</p>
            </div>
          ))}
        </div>
      </div>
    )
  }

  const PricingType = ({ type }) => {
    return (
      <div className='w-[50%] flex flex-col items-center rounded-3xl border-[0.5px] border-[#454343] p-6 gap-5'>
        <div className='w-[90%] flex justify-between items-center'>
          <button
            className='rounded-lg border-none text-white text-base font-bold font-["Roboto"] cursor-pointer bg-[#ffffff1a] px-6 py-2'
            id='btn-free'>{type}</button>
          <button
            className='rounded-lg border-none text-white text-base font-bold font-["Roboto"] cursor-pointer bg-[#4a5fa2] px-6 py-2'
            id='btn-buy'>Buy Now</button>
        </div>
        <div className='w-full border-b-[#686868] font-["Roboto"] text-white px-6'>
          <h3 className='text-xl leading-8 font-bold'>$0.00</h3>
          <p className='text-sm font-medium leading-5 text-[#838383]'>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
        </div>
        <div className='flex flex-col items-center gap-4 px-6'>
          {new Array(5).fill('').map((_, index) => (
            <div className='pricing-item-list-data' key={index}>
              <img src={checkIcon} alt='check-icon' />
              <p>Lorem ipsum, dolor sit amet consectetur</p>
            </div>
          ))}
        </div>
      </div>
    )
  }

  return (
    <div className='w-full flex'>
      <Sidebar />
      <div className='w-full ml-[20%] px-5 pb-64 bg-[#0d0d19] flex flex-col items-center font-outfit'>
        <div className='w-[98%] flex justify-between items-center py-4'>
          <SearchBox />
          <div className='w-[50%] flex flex-col justify-between items-center'>
            <h2 className='text-5xl leading-[3.5rem] font-semibold text-white'>Zummit Infolabs</h2>
            <p className='text-xl font-medium text-[#9c9c9c]'>Invent Your Future Today</p>
          </div>
          <div className='w-[18%] flex flex-col items-center text-[#9c9c9c] text-[1rem] leading-5 font-medium'>
            <p>Visit Our Page</p>
            <img className='w-20' src={zlogo} alt="zummit-logo" />
          </div>
        </div>
        <div className='w-[98%] flex justify-center'>
          <img src={bannerImage} alt="banner-image" />
        </div>
        <BannerHome />
        <CarouselElement data={models} type="models" />
        <CarouselElement data={tools} type="tools" />
        <SpecialArticle />
        <div className='pricing-main w-[98%] flex justify-center items-center gap-8 rounded-3xl border-[0.5px] border-[#454343] p-8 my-4'>
          <PricingType type='Free' />
          <PricingType type='Exclusive' />
        </div>
        <Testimonials />
      </div>
    </div>
  )
}

const elements = [
  {
    id: 1,
    name: 'Emotion Detection',
    desc: 'Racism and Terrorism detection are some of the key use cases around emotion detection. Uses Deep Neural Networks and architectures similar to AlexNet under Computer Vision.',
    img: emotionDetection
  },
  {
    id: 2,
    name: 'Time Series Analysis',
    desc: 'Analysis of Time Series data is quite an interesting area, especially when dealing with use cases like Stock Market Predictions, Forex, Crypto and the like',
    img: timeAnalysis

  },
  {
    id: 3,
    name: 'Reinforcement Learning',
    desc: 'Racism and Terrorism detection are some of the key use cases around emotion detection. Uses Deep Neural Networks and architectures similar to AlexNet under Computer Vision.',
    img: reinforcement

  },
  {
    id: 4,
    name: 'Language Processing',
    desc: 'Analysis of Time Series data is quite an interesting area, especially when dealing with use cases like Stock Market Predictions, Forex, Crypto and the like',
    img: language

  },
  {
    id: 5,
    name: 'Data Analytics',
    desc: 'One of the key benefits or outcome of AI/ML solutions is the ability to provide analytics, be it, Preditive Analytics, Prescriptive Analytics or even Descriptive Analytics.',
    img: analytics

  },
  {
    id: 6,
    name: 'Anomaly Detection',
    desc: 'Fraud detection is one of the best examples of detection anomalous transactions in Banking and financial applications. Prescriptive analytics in ecommerce, healthcare disease data and more.',
    img: anomaly

  },
  {
    id: 7,
    name: 'Generative Solutions',
    desc: 'One of the most cutting edge use cases around Fashion domain like Hairstyle suggestions, apparel design, 3D models of house designs, Anime characters generation and more.',
    img: generativeSol

  },
  {
    id: 8,
    name: 'Machine Learning',
    desc: 'Racism and Terrorism detection are some of the key use cases around emotion detection. Uses Deep Neural Networks and architectures similar to AlexNet under Computer Vision.',
    img: machineLearning

  }
]

export default Home