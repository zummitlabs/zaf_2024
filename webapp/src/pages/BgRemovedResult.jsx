import React, { useEffect, useRef, useState } from 'react';
import bgOption2 from '../assets/bgOption2.jpg';
import bgOption3 from '../assets/bgOption3.png';
import bgOption4 from '../assets/bgOption4.png';
import bgOption5 from '../assets/bgOption5.png';
import bgOption6 from '../assets/bgOption6.png';
import bgOption7 from '../assets/bgOption7.png';
import bgOption8 from '../assets/bgOption8.png';
import bgOption9 from '../assets/bgOption9.png';
import bgOption10 from '../assets/bgOption10.png';
import bgOption11 from '../assets/bgOption11.png';
import bgOption12 from '../assets/bgOption12.png';
import bgOption13 from '../assets/bgOption13.png';
import bgOption14 from '../assets/bgOption14.png';
import bgOption15 from '../assets/bgOption15.jpg';
import bgOption16 from '../assets/bgOption16.png';
import bgGrid from '../assets/bgGrid.png';
import bgRemoveResult from '../assets/bgRemoveResult.png';
import '../App.css';

const BgRemovedResult = () => {
    const [uploads, setUploads] = useState([bgRemoveResult]);
    const bgOptions = [
        {
            id: 1,
            img: bgGrid,
            name: "Transparent",
            upload: false
        },
        {
            id: 2,
            img: bgOption3,
            name: "Upload",
            upload: true
        },
        {
            id: 3,
            img: bgOption2,
            name: "Colors",
            upload: false
        },
        {
            id: 4,
            img: bgOption14,
            name: "White",
            upload: false
        },
        {
            id: 5,
            img: bgOption15,
            name: "Black",
            upload: false
        },
        {
            id: 6,
            img: bgOption16,
            name: "Red",
            upload: false
        },
        {
            id: 7,
            img: bgOption4,
            name: "Wall",
            upload: false
        },
        {
            id: 8,
            img: bgOption13,
            name: "Space",
            upload: false
        },
        {
            id: 9,
            img: bgOption12,
            name: "Flowers",
            upload: false
        },
        {
            id: 10,
            img: bgOption11,
            name: "City",
            upload: false
        },
        {
            id: 11,
            img: bgOption10,
            name: "Cave",
            upload: false
        },
        {
            id: 12,
            img: bgOption9,
            name: "Monument",
            upload: false
        },
        {
            id: 13,
            img: bgOption8,
            name: "Beach",
            upload: false
        },
        {
            id: 14,
            img: bgOption7,
            name: "Grass",
            upload: false
        },
        {
            id: 15,
            img: bgOption6,
            name: "Desert",
            upload: false
        },
        {
            id: 16,
            img: bgOption5,
            name: "Pattern",
            upload: false
        },
    ]
    const inputImage = useRef(null);
    const [image, setImage] = useState(null);
    const [background, setBackground] = useState(bgOptions[0]);
    const [currentView, setCurrentView] = useState(uploads[0]);
    const [error, setError] = useState("");

    const bgInput = useRef(null);
    const [bgUpload, setBgUpload] = useState();

    const [toggleBg, setToggleBg] = useState(false);
    const [toggleEffects, setToggleEffects] = useState(false);

    const handleChange = (e) => {
        setImage(URL.createObjectURL(e.target.files[0]));
        if (uploads.length < 5) {
            setUploads(prev => [...prev, URL.createObjectURL(e.target.files[0])]);
            setCurrentView(URL.createObjectURL(e.target.files[0]))
        } else {
            setError("Limit Reached")
        }
    };
    console.log(uploads)

    const handleBackgroundChange = (bg) => {
        setBgUpload(null);
        setBackground(bg)
    };

    const handleBgUpload = (e) => {
        setBgUpload(URL.createObjectURL(e.target.files[0]));
    }

    const handleResultView = (index) => {
        setCurrentView(uploads[index]);
    }

    return (
        <div className='w-full min-h-[100vh] flex flex-col items-center gap-3 font-outfit p-6 bg-[#000617]'>
            <h2 className='text-white text-[2.5rem]'>Upload more images</h2>
            <div className='w-full flex flex-col gap-5 items-center text-center py-4'>
                <p className='text-red-600 bg-white rounded-md px-5 py-2 absolute top-[50%]' style={error ? { display: "block" } : { display: "none" }}>{error}</p>
                <button
                    className='flex items-center gap-3 bg-[#4A5FA2] rounded-xl text-white px-7 py-2 text-lg font-semibold'
                    onClick={() => inputImage.current.click()}
                >
                    <svg width="16" height="21" viewBox="0 0 18 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6.66665 15.6668H11.3333C11.975 15.6668 12.5 15.1418 12.5 14.5001V8.66677H14.355C15.3933 8.66677 15.9183 7.40677 15.1833 6.67177L9.82831 1.31677C9.37331 0.861775 8.63831 0.861775 8.18331 1.31677L2.82831 6.67177C2.09331 7.40677 2.60665 8.66677 3.64498 8.66677H5.49998V14.5001C5.49998 15.1418 6.02498 15.6668 6.66665 15.6668ZM1.99998 18.0001H16C16.6416 18.0001 17.1666 18.5251 17.1666 19.1668C17.1666 19.8084 16.6416 20.3334 16 20.3334H1.99998C1.35831 20.3334 0.833313 19.8084 0.833313 19.1668C0.833313 18.5251 1.35831 18.0001 1.99998 18.0001Z" fill="white" />
                    </svg>
                    Upload Image
                </button>
                <input type="file" ref={inputImage} onChange={handleChange} style={{ display: 'none' }} />
            </div>
            <div className='w-full flex justify-center gap-10'>
                {/* Uploaded Images */}
                <div className='flex flex-col gap-5'>
                    {uploads.map((upload, index) => (
                        <img className='w-12 h-12 rounded-full border-[2px] border-white' src={upload} alt="uploadedImage" onClick={() => handleResultView(index)} />
                    ))}
                    <div
                        className='w-12 h-12 bg-[#4A5FA2] border-[2px] border-white rounded-full flex items-center justify-center text-4xl font-medium text-white cursor-pointer'
                        onClick={() => inputImage.current.click()}>
                        +
                    </div>
                </div>

                <div className='bannerhome-main w-[60%] flex border-[0.5px] border-[#9c9c9c] rounded-[1.5rem] p-10 gap-10'>
                    <div className='rounded-3xl w-[60%]' style={{ backgroundImage: `url(${bgUpload ? bgUpload : background.img})`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }}>
                        <img className='rounded-3xl' src={currentView} alt="image" />
                    </div>
                    <div className='w-[40%] text-white flex flex-col justify-between p-2'>
                        {/* Selectors */}
                        <div className='w-full flex flex-col gap-4 relative'>
                            <div
                                className='flex items-center gap-4 cursor-pointer'
                                onClick={() => setToggleBg(true)}
                            >
                                <p>Background</p>
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8.72954 12.0219C8.33447 12.4433 7.66554 12.4433 7.27046 12.0219L1.5787 5.95067C0.979948 5.312 1.43279 4.26672 2.30823 4.26672H13.6918C14.5672 4.26672 15.0201 5.312 14.4213 5.95066L8.72954 12.0219Z" fill="white" />
                                </svg>
                            </div>
                            <div className='flex gap-3 items-center'>
                                <img className='w-10 h-10 rounded-md border-[3px] border-[#3557C6]' src={bgUpload ? bgUpload : background.img} />
                                <p>{bgUpload ? bgUpload : background.name}</p>
                            </div>

                            {/* Bg options toggled */}
                            {toggleBg ? (
                                <div className='absolute top-[5.5rem] bg-[#C1C1C1] w-[20rem] h-64 rounded-2xl overflow-y-auto scroll-bar z-10'>
                                    <div className='bg-gray-400 w-full px-5 py-2 flex justify-between items-center'>
                                        <p className=''>Select Background</p>
                                        <svg onClick={() => setToggleBg(false)} className='cursor-pointer' width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M8.46411 15.5348L15.5361 8.46484M8.46411 8.46484L15.5361 15.5348" stroke="black" stroke-width="1.5" stroke-linecap="round" />
                                        </svg>
                                    </div>
                                    <div className='flex flex-wrap gap-7 px-5 py-5'>
                                        {bgOptions.map((bg) => (
                                            <div key={bg.id} className='cursor-pointer'>
                                                {bg.upload
                                                    ? <>
                                                        <input type='file' ref={bgInput} onChange={handleBgUpload} style={{ display: 'none' }} />
                                                        <img className='w-10 h-10 rounded-md' src={bg.img} onClick={() => bgInput.current.click()} />
                                                    </>
                                                    : <img onClick={() => handleBackgroundChange(bg)} className='w-10 h-10 rounded-md' src={bg.img} alt="bgOption" />}
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            ) : (null)}

                            <div className='flex items-center gap-4 cursor-pointer'>
                                <div
                                    className='flex items-center gap-4 cursor-pointer'
                                    onClick={() => setToggleEffects(true)}
                                >
                                    <p>Effects</p>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M8.72954 12.0219C8.33447 12.4433 7.66554 12.4433 7.27046 12.0219L1.5787 5.95067C0.979948 5.312 1.43279 4.26672 2.30823 4.26672H13.6918C14.5672 4.26672 15.0201 5.312 14.4213 5.95066L8.72954 12.0219Z" fill="white" />
                                    </svg>
                                </div>
                                {/* Effects toggled */}
                                {toggleEffects ? (
                                    <div className='absolute top-[8.5rem] bg-[#C1C1C1] w-[20rem] h-64 rounded-2xl overflow-y-auto scroll-bar z-10'>
                                        <div className='bg-gray-400 w-full px-5 py-2 flex justify-between items-center'>
                                            <p className=''>Select Effect</p>
                                            <svg onClick={() => setToggleEffects(false)} className='cursor-pointer' width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.46411 15.5348L15.5361 8.46484M8.46411 8.46484L15.5361 15.5348" stroke="black" stroke-width="1.5" stroke-linecap="round" />
                                            </svg>
                                        </div>
                                        <div className='flex flex-col gap-7 px-5 py-5'>
                                            <div>
                                                <p>Opacity</p>
                                                <input type="range" name="opacity" min={0} max={100} defaultValue={0} />
                                            </div>
                                            <div>
                                                <p>Blur</p>
                                                <input type="range" name="blur" min={0} max={100} defaultValue={0} />
                                            </div>
                                        </div>
                                    </div>
                                ) : (null)}

                            </div>
                        </div>

                        {/* Buttons */}
                        <div className='w-full flex flex-col gap-4 font-outfit'>

                            <button className='bg-[#4A5FA2] rounded-lg text-white font-semibold text-xl py-2'>Download <span className='text-sm font-normal'>&#40;72 DPI&#41;</span></button>
                            <button className='bg-[#4A5FA2] rounded-lg text-white font-semibold text-xl py-2'>Download HD <span className='text-sm font-normal'>&#40;300 DPI&#41;</span></button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BgRemovedResult