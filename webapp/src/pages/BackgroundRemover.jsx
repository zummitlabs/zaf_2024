import React, { useEffect, useRef, useState } from 'react';
import '../App.css';
import bgRemoverImg from '../assets/bgRemoverImg.png';
import bgRemoverImg3 from '../assets/bgRemoverImg3.png';
import bgRemoverImg4 from '../assets/bgRemoverImg4.png';
import bgRemoverImg5 from '../assets/bgRemoverImg5.png';
import bgRemoverImg8 from '../assets/bgRemoveImg8.png';
import people1 from '../assets/bgRemover/samples/people1.png';
import people2 from '../assets/bgRemover/samples/people2.png';
import graphics1 from '../assets/bgRemover/samples/graphics1.png';
import graphics2 from '../assets/bgRemover/samples/graphics2.png';
import animals1 from '../assets/bgRemover/samples/animals1.png';
import animals2 from '../assets/bgRemover/samples/animals2.png';
import cars1 from '../assets/bgRemover/samples/cars1.png';
import cars2 from '../assets/bgRemover/samples/cars2.png';
import products1 from '../assets/bgRemover/samples/products1.png';
import products2 from '../assets/bgRemover/samples/products2.png';
import people21 from '../assets/bgRemover/samples/people2.1.png';
import people22 from '../assets/bgRemover/samples/people2.2.png';
import animals21 from '../assets/bgRemover/samples/animals2.1.png';
import animals22 from '../assets/bgRemover/samples/animals2.2.png';
import cars21 from '../assets/bgRemover/samples/cars2.1.png';
import cars22 from '../assets/bgRemover/samples/cars2.2.png';
import products21 from '../assets/bgRemover/samples/products2.1.png';
import products22 from '../assets/bgRemover/samples/products2.2.png';
import graphics21 from '../assets/bgRemover/samples/graphics2.1.png';
import graphics22 from '../assets/bgRemover/samples/graphics2.2.png';
import clientImage from '../assets/carouselImages/clientImage.jpg';
import fiveseconds from '../assets/fiveseconds.png';

import { ReactCompareSlider, ReactCompareSliderImage } from 'react-compare-slider';
import { useNavigate } from 'react-router-dom';
import { useDropzone } from 'react-dropzone';
import Slider from 'react-slick';

// Background Remover
const BackgroundRemover = () => {
    const [sampleSelectedTab1, setSampleSelectedTab1] = useState(0);
    const [sampleSelectedTab2, setSampleSelectedTab2] = useState(0);
    const navigate = useNavigate();

    const Uploader = () => {
        const [image, setImage] = useState();

        const { getRootProps, getInputProps } = useDropzone({
            onDrop: (acceptedFiles) => {
                setImage(acceptedFiles[0]);
                navigate('/bg-result')
            },
        });
        console.log(image)

        return (
            <div
                className='w-full flex flex-col items-center gap-14 border-2 border-dashed border-[#4A5FA2] rounded-3xl font-outfit p-8 my-10'
                {...getRootProps()}
            >
                <svg width="81" height="80" viewBox="0 0 81 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M33.0486 2.94286L70.2629 8.17143C72.5137 8.48801 74.5466 9.68565 75.9145 11.5009C77.2824 13.3162 77.8733 15.6005 77.5571 17.8514L72.3286 55.0429C72.172 56.1576 71.7974 57.2306 71.2261 58.2006C70.6548 59.1706 69.898 60.0185 68.999 60.696C68.1 61.3735 67.0763 61.8672 65.9864 62.1491C64.8966 62.4309 63.7619 62.4953 62.6471 62.3386L60.4914 62.0357V31.4586C60.4914 28.4275 59.2874 25.5206 57.1441 23.3774C55.0008 21.2341 52.0939 20.03 49.0629 20.03H21.9914L23.3671 10.2371C23.5237 9.12238 23.8984 8.04937 24.4697 7.0794C25.0409 6.10943 25.7977 5.2615 26.6967 4.58402C27.5957 3.90653 28.6194 3.41278 29.7093 3.13094C30.7991 2.8491 31.9338 2.78613 33.0486 2.94286ZM19.1057 20.0286L20.5371 9.84C20.9592 6.83852 22.5564 4.12764 24.9772 2.30372C27.398 0.479796 30.4442 -0.307766 33.4457 0.114285L70.66 5.34286C73.6615 5.76462 76.3725 7.3614 78.1966 9.78194C80.0208 12.2025 80.8088 15.2485 80.3871 18.25L75.1586 55.4429C74.9498 56.9293 74.4501 58.3601 73.6883 59.6534C72.9265 60.9468 71.9173 62.0773 70.7185 62.9806C69.5196 63.8839 68.1546 64.5422 66.7013 64.9178C65.248 65.2935 63.735 65.3792 62.2486 65.17L60.4914 64.9229V68.5714C60.4914 71.6025 59.2874 74.5094 57.1441 76.6527C55.0008 78.7959 52.0939 80 49.0629 80H11.9286C8.89753 80 5.99062 78.7959 3.84735 76.6527C1.70408 74.5094 0.5 71.6025 0.5 68.5714V31.4571C0.5 28.4261 1.70408 25.5192 3.84735 23.3759C5.99062 21.2327 8.89753 20.0286 11.9286 20.0286H19.1057ZM29.6386 38.5914C29.487 38.5914 29.3417 38.6516 29.2345 38.7588C29.1273 38.866 29.0671 39.0113 29.0671 39.1629V48.5872H19.64C19.4884 48.5872 19.3431 48.6474 19.2359 48.7545C19.1288 48.8617 19.0686 49.007 19.0686 49.1586V50.8729C19.0686 51.1871 19.3257 51.4443 19.64 51.4443H29.0686V60.8657C29.0686 61.1814 29.3229 61.4371 29.64 61.4371H31.3543C31.5058 61.4371 31.6512 61.3769 31.7583 61.2698C31.8655 61.1626 31.9257 61.0173 31.9257 60.8657V51.4443H41.3514C41.503 51.4443 41.6483 51.3841 41.7555 51.2769C41.8627 51.1698 41.9229 51.0244 41.9229 50.8729V49.1586C41.9229 49.007 41.8627 48.8617 41.7555 48.7545C41.6483 48.6474 41.503 48.5872 41.3514 48.5872H31.9243V39.1629C31.9243 39.0113 31.8641 38.866 31.7569 38.7588C31.6498 38.6516 31.5044 38.5914 31.3529 38.5914H29.6386ZM11.9286 22.8871H49.0629C51.3361 22.8871 53.5163 23.7902 55.1238 25.3977C56.7312 27.0051 57.6343 29.1853 57.6343 31.4586V68.5714C57.6343 70.8447 56.7312 73.0249 55.1238 74.6324C53.5163 76.2398 51.3361 77.1429 49.0629 77.1429H11.9286C9.65529 77.1429 7.47511 76.2398 5.86766 74.6324C4.2602 73.0249 3.35714 70.8447 3.35714 68.5714V31.4571C3.35714 29.1839 4.2602 27.0037 5.86766 25.3962C7.47511 23.7888 9.65529 22.8871 11.9286 22.8871Z" fill="white" />
                </svg>
                <div className='w-[65%] flex flex-col gap-10 items-center text-center'>
                    <button
                        className='flex items-center gap-4 bg-[#4A5FA2] rounded-xl text-white px-7 py-2 text-lg font-semibold'

                    >
                        <svg width="16" height="21" viewBox="0 0 18 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.66665 15.6668H11.3333C11.975 15.6668 12.5 15.1418 12.5 14.5001V8.66677H14.355C15.3933 8.66677 15.9183 7.40677 15.1833 6.67177L9.82831 1.31677C9.37331 0.861775 8.63831 0.861775 8.18331 1.31677L2.82831 6.67177C2.09331 7.40677 2.60665 8.66677 3.64498 8.66677H5.49998V14.5001C5.49998 15.1418 6.02498 15.6668 6.66665 15.6668ZM1.99998 18.0001H16C16.6416 18.0001 17.1666 18.5251 17.1666 19.1668C17.1666 19.8084 16.6416 20.3334 16 20.3334H1.99998C1.35831 20.3334 0.833313 19.8084 0.833313 19.1668C0.833313 18.5251 1.35831 18.0001 1.99998 18.0001Z" fill="white" />
                        </svg>
                        Upload Image
                    </button>
                    <input type="file" {...getInputProps()} style={{ display: 'none' }} />
                    <p className='w-[75%] text-[#C8C8C8] text-[1rem] leading-5 font-normal'>Drag and Drop or Paste Image &#40;Ctrl + V&#41;</p>
                </div>
            </div>
        )
    };

    const Banner = () => {
        return (
            <div className='w-full flex items-center gap-64 justify-center font-outfit p-7'>
                <div className='w-[40%] text-white flex flex-col gap-7 items-center'>
                    <div className='text-[2.8rem] font-semibold'>
                        <div className='flex items-center gap-2'>
                            <svg width="100" height="100" viewBox="0 0 101 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_246_374)">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M30.5657 63.007L33.2469 61.7567L26.9956 48.3509C24.9241 43.9086 26.8464 38.6281 31.2884 36.5567L63.4623 21.5538C67.9042 19.4825 73.185 21.4042 75.2565 25.8465L81.5077 39.2523L84.1889 38.0021C85.6694 37.3117 87.4298 37.9522 88.1203 39.433C88.8108 40.9138 88.1699 42.674 86.6894 43.3644L84.0082 44.6146L90.2595 58.0204C92.331 62.4629 90.4086 67.7434 85.9667 69.8147L53.7928 84.8176C49.3508 86.8889 44.0701 84.9673 41.9986 80.5249L35.7473 67.1191L33.0662 68.3693C31.5855 69.0598 29.8253 68.4192 29.1348 66.9384C28.4443 65.4576 29.085 63.6974 30.5657 63.007ZM65.9628 26.9161L33.7889 41.919C32.3082 42.6095 31.6674 44.3696 32.3579 45.8505L38.6092 59.2563L76.1454 41.7528L69.8942 28.347C69.2037 26.8662 67.4433 26.2257 65.9628 26.9161ZM71.3877 50.4997L64.4548 53.7326L55.8693 77.321L62.8023 74.0881L71.3877 50.4997ZM66.594 72.32L75.1794 48.7316L78.6459 47.1151L80.2624 50.5816L73.5269 69.0871L66.594 72.32ZM77.3186 67.319L83.4662 64.4523C84.9468 63.762 85.5877 62.0017 84.8971 60.5209L82.0305 54.3733L77.3186 67.319ZM52.0776 79.0891L60.6631 55.5007L53.7302 58.7335L46.9947 77.2391L47.3609 78.0244C48.0514 79.5052 49.8116 80.1457 51.2923 79.4553L52.0776 79.0891ZM45.2266 73.4474L49.9384 60.5017L41.1097 64.6186L45.2266 73.4474Z" fill="white" />
                                    <path d="M19.8769 40.0825L20.7926 46.8699L16.2451 41.7761L9.45766 42.6918L14.5515 38.1443L13.5421 31.3211L18.1833 36.4507L24.9413 35.4717L19.8769 40.0825ZM28.6856 32.0177L29.1893 35.4298L26.7728 32.9096L23.3945 33.3206L25.9104 31.0601L25.5626 27.6523L27.8231 30.1681L31.172 29.6939L28.6856 32.0177ZM15.9709 50.67L16.5103 53.9884L14.2498 51.4725L10.8715 51.8835L13.3873 49.623L12.8836 46.2109L15.1084 48.8204L18.5837 48.2873L15.9709 50.67ZM44.6263 34.5943L46.9795 37.1439L50.3283 36.6697L47.842 38.9934L48.253 42.3718L45.9292 39.8854L42.5509 40.2964L45.0667 38.0358L44.6263 34.5943Z" fill="white" />
                                </g>
                                <defs>
                                    <clipPath id="clip0_246_374">
                                        <rect width="76" height="76" fill="white" transform="translate(0 32.119) rotate(-25)" />
                                    </clipPath>
                                </defs>
                            </svg>
                            <h2>Automatic</h2>
                        </div>
                        <h2>Background Removal</h2>
                    </div>
                    <p className='text-[#C8C8C8] text-base text-center leading-5'>Automatically clear backgrounds and spotlight the subject of your photos in a few simple steps. Quick, effortless, and professional-grade results!</p>
                    <div className='w-[55%] flex flex-col gap-8'>
                        <p className='text-[#C8C8C8] font-semibold text-lg text-center'>Fast and Free</p>
                        <div className='flex -rotate-3 w-full'>
                            {/* <img className='w-[82%]' src={bgRemoverImg5} alt="bgImg5" /> */}

                            <ReactCompareSlider
                                ref={{
                                    current: '[Circular]'
                                }}
                                boundsPadding={0}
                                itemOne={<ReactCompareSliderImage className='rounded-3xl' alt="Image one" src={bgRemoverImg5} />}
                                itemTwo={<ReactCompareSliderImage alt="Image two" className='rounded-3xl' src={bgRemoverImg8} style={{ backgroundColor: 'white', backgroundImage: '\n linear-gradient(45deg, #ccc 25%, transparent 25%),\n linear-gradient(-45deg, #ccc 25%, transparent 25%),\n linear-gradient(45deg, transparent 75%, #ccc 75%),\n linear-gradient(-45deg, transparent 75%, #ccc 75%)', backgroundPosition: '0 0, 0 10px, 10px -10px, -10px 0px', backgroundSize: '20px 20px' }} />}
                                keyboardIncrement="5%"
                                position={50}
                                style={{
                                    height: '50vh',
                                    width: '100%',
                                }}
                            />
                        </div>
                    </div>
                </div>
                <div className='w-[29%] flex flex-col gap-6'>
                    <Uploader />
                    <p className='text-center text-white text-sm'>By uploading an image, you agree to our Terms of Service. For details on how ZAF manages your personal data,
                        please refer to our Privacy Policy.</p>
                    <div className='flex flex-col gap-3 items-center'>
                        <p className='text-center text-[#C8C8C8] text-sm leading-5 font-semibold'>No Image?<br />Explore these alternatives:</p>
                        <img src={bgRemoverImg4} alt="bgImg4" />
                    </div>
                </div>
            </div>
        )
    };

    const SlidingSamples = () => {
        const tabs = [
            {
                name: "People",
                img1: people1,
                img2: people2
            },
            {
                name: "Products",
                img1: products1,
                img2: products2
            },
            {
                name: "Animals",
                img1: animals1,
                img2: animals2
            },
            {
                name: "Cars",
                img1: cars1,
                img2: cars2
            },
            {
                name: "Graphics",
                img1: graphics1,
                img2: graphics2
            }
        ]
        return (
            <div className='flex flex-col items-center justify-between font-outfit gap-7 my-20'>
                <h2 className='text-white text-6xl font-medium'>Impressive Quality</h2>
                <div className='flex gap-6'>
                    {tabs.map((tab, index) => (
                        <button
                            key={index}
                            className={`text-white font-semibold rounded-full px-4 py-2 ${sampleSelectedTab1 === index ? 'bg-[#3557C6]' : 'bg-[#4A5FA2]'}`}
                            onClick={() => setSampleSelectedTab1(index)}
                        >
                            {tab.name}
                        </button>
                    ))}
                </div>
                <div className='w-[50%] flex flex-col items-center'>
                    {tabs.map((tab, index) => (
                        <div className={`${sampleSelectedTab1 === index ? "" : "hidden"}`}>
                            <ReactCompareSlider
                                key={index}
                                boundsPadding={0}
                                itemOne={<ReactCompareSliderImage className='rounded-3xl' alt="Image one" src={tab.img1} />}
                                itemTwo={<ReactCompareSliderImage alt="Image two" className='rounded-3xl' src={tab.img2} style={{ backgroundColor: 'white', backgroundImage: '\n linear-gradient(45deg, #ccc 25%, transparent 25%),\n linear-gradient(-45deg, #ccc 25%, transparent 25%),\n linear-gradient(45deg, transparent 75%, #ccc 75%),\n linear-gradient(-45deg, transparent 75%, #ccc 75%)', backgroundPosition: '0 0, 0 10px, 10px -10px, -10px 0px', backgroundSize: '20px 20px' }} />}
                                keyboardIncrement="5%"
                                position={97}
                                style={{
                                    height: '75vh',
                                    width: '100%',
                                }}
                            />
                        </div>
                    ))}
                </div>
            </div>
        )
    };

    const Samples = () => {
        const samples = [
            {
                name: "People",
                img1: people21,
                img2: people22,
                img3: people22,
            },
            {
                name: "Products",
                img1: products21,
                img2: products22,
                img3: products22,
            },
            {
                name: "Animals",
                img1: animals21,
                img2: animals22,
                img3: animals22,
            },
            {
                name: "Cars",
                img1: cars21,
                img2: cars22,
                img3: cars22,
            },
            {
                name: "Graphics",
                img1: graphics21,
                img2: graphics22,
                img3: graphics22,
            },
        ]
        return (
            <div className='flex flex-col items-center justify-between font-outfit gap-7 my-20'>
                <h2 className='text-white text-6xl font-medium'>Visualize It!</h2>
                <div className='flex gap-6'>
                    {samples.map((tab, index) => (
                        <button
                            key={index}
                            className={`text-white font-semibold rounded-full px-4 py-2 ${sampleSelectedTab2 === index ? 'bg-[#3557C6]' : 'bg-[#4A5FA2]'}`}
                            onClick={() => setSampleSelectedTab2(index)}
                        >
                            {tab.name}
                        </button>
                    ))}
                </div>
                <div className='w-[50%] flex flex-col items-center'>
                    {samples.map((tab, index) => (
                        <div className={`flex gap-5 justify-center ${sampleSelectedTab2 === index ? "" : "hidden"}`}>
                            <img className='w-[40%] rounded-lg' src={tab.img1} alt="img1" />
                            <img className='w-[40%] rounded-lg' src={tab.img2} alt="img2" style={{ backgroundColor: 'white', backgroundImage: '\n linear-gradient(45deg, #ccc 25%, transparent 25%),\n linear-gradient(-45deg, #ccc 25%, transparent 25%),\n linear-gradient(45deg, transparent 75%, #ccc 75%),\n linear-gradient(-45deg, transparent 75%, #ccc 75%)', backgroundPosition: '0 0, 0 10px, 10px -10px, -10px 0px', backgroundSize: '20px 20px' }} />
                            <img className='w-[40%] rounded-lg bg-[#7DBDD1]' src={tab.img3} alt="img3" />
                        </div>
                    ))}
                </div>
            </div>
        )
    };

    const FiveSecondInfo = () => {
        return (
            <div className='w-3/5 mx-auto flex gap-20 my-10 p-5'>
                <div className='flex flex-col justify-between w-2/4'>
                    <h2 className='text-3xl font-semibold text-white'>Background removal done automatically in 5 seconds with one click</h2>
                    <p className='text-xl text-white leading-7 font-normal'>This complimentary background removal tool enables you to emphasize the subject of your picture and
                        generate a transparent background, allowing you to integrate your new image into various designs and
                        settings. Position it against a coloured backdrop or insert a new background to situate your subject
                        in an entirely different environment.</p>
                </div>
                <div>
                    <img src={fiveseconds} alt={fiveseconds} />
                </div>
            </div>
        )
    };

    const TestimonialCarousel = () => {
        const clients = [
            {
                id: 1,
                name: 'Amy Watson',
                stars: 5,
                img: clientImage,
                review: '"This service is a game-changer for me as a freelance photographer, saving time and ensuring top-notch client results."'
            },
            {
                id: 2,
                name: 'Amy Watson',
                stars: 4,
                img: clientImage,
                review: '"This service is a game-changer for me as a freelance photographer, saving time and ensuring top-notch client results."'
            },
            {
                id: 3,
                name: 'Amy Watson',
                stars: 5,
                img: clientImage,
                review: '"This service is a game-changer for me as a freelance photographer, saving time and ensuring top-notch client results."'
            },
            {
                id: 4,
                name: 'Amy Watson',
                stars: 4,
                img: clientImage,
                review: '"This service is a game-changer for me as a freelance photographer, saving time and ensuring top-notch client results."'
            },
            {
                id: 5,
                name: 'Amy Watson',
                stars: 5,
                img: clientImage,
                review: '"This service is a game-changer for me as a freelance photographer, saving time and ensuring top-notch client results."'
            },

        ]
        const [oldSlide, setOldSlide] = useState(0);
        const [activeSlide, setActiveSlide] = useState(0);
        const [activeSlide2, setActiveSlide2] = useState(0);

        const settings = {
            arrows: false,
            infinite: true,
            speed: 500,
            slidesToShow: clients.length % 3,
            slidesToScroll: 1,
            arrows: false,
            beforeChange: (current, next) => {
                setOldSlide(current);
                setActiveSlide(next);
            },
            afterChange: current => setActiveSlide2(current)
        }
        return (
            <div className='w-full flex flex-col items-center p-4'>
                <h2 className='text-white text-6xl font-medium mb-4'>Success Stories</h2>
                <div className='border-[0.5px] border-[#464646] rounded-3xl w-[85%] h-[24rem] bg-[#00000023] p-6'>
                    <Slider {...settings}>
                        {clients.map((client, index) => (
                            <div key={client.id} className={`bg-white h-[240px] p-5 ${activeSlide === index && 'h-[325px] p-8'} rounded-3xl border-[#0258A2] border-[2px]`}>
                                <div className={`flex flex-col ${activeSlide === index ? 'gap-10' : 'gap-6'}`}>
                                    <div className='flex gap-8 items-center'>
                                        <img className={`h-[95px] w-[95px] rounded-full object-cover`} src={client.img} />
                                        <h2 className={`text-xl font-semibold text-[#0258A2] ${activeSlide === index && 'text-2xl'}`}>{client.name}</h2>
                                    </div>
                                    <div className='w-[96%]'>
                                        <p className={`${activeSlide === index ? 'text-lg' : 'text-base'} font-medium font-outfit`}>{client.review}</p>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </Slider>
                </div>
            </div>
        )
    };

    return (
        <div className='w-full h-100% bg-[#000617]'>
            <Banner />
            <SlidingSamples />
            <FiveSecondInfo />
            <Samples />
            <TestimonialCarousel />
            {/* Main */}
            <div className='py-5 px-4'>
                {/* Top */}
                <div className='flex flex-col items-center text-white py-5 gap-2'>
                    <div className='flex items-center gap-4 font-outfit'>
                        <svg width="75" height="65" viewBox="0 0 75 66" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6481 36.4117H16.6064V21.62C16.6064 16.7185 20.5802 12.745 25.4814 12.745H60.9814C65.8825 12.745 69.8564 16.7185 69.8564 21.62V36.4117H72.8148C74.4484 36.4117 75.7731 37.7361 75.7731 39.37C75.7731 41.0039 74.4484 42.3283 72.8148 42.3283H69.8564V57.12C69.8564 62.0217 65.8825 65.995 60.9814 65.995H25.4814C20.5802 65.995 16.6064 62.0217 16.6064 57.12V42.3283H13.6481C12.0144 42.3283 10.6898 41.0039 10.6898 39.37C10.6898 37.7361 12.0144 36.4117 13.6481 36.4117ZM60.9814 18.6617H25.4814C23.8477 18.6617 22.5231 19.9861 22.5231 21.62V36.4117H63.9398V21.62C63.9398 19.9861 62.615 18.6617 60.9814 18.6617ZM55.9313 42.3283H48.2816L30.5316 60.0783H38.1813L55.9313 42.3283ZM42.3649 60.0783L60.1149 42.3283H63.9398V46.1532L50.0146 60.0783H42.3649ZM54.1983 60.0783H60.9814C62.615 60.0783 63.9398 58.7539 63.9398 57.12V50.3368L54.1983 60.0783ZM26.3479 60.0783L44.0979 42.3283H36.4483L22.5231 56.2535V57.12C22.5231 58.7539 23.8477 60.0783 25.4814 60.0783H26.3479ZM22.5231 52.0698L32.2646 42.3283H22.5231V52.0698Z" fill="white" />
                            <path d="M13.649 11.1177L11.6105 17.6562L9.64177 11.1177L3.10328 9.07924L9.64177 7.11049L11.6105 0.499995L13.649 7.11049L20.1875 9.07924L13.649 11.1177ZM25.0408 7.53124L24.0553 10.8365L22.9303 7.53124L19.6948 6.47599L22.9303 5.49049L24.0553 2.25499L25.0408 5.49049L28.2763 6.47599L25.0408 7.53124ZM5.63453 19.0625L4.72103 22.298L3.73553 19.0625L0.500026 18.0072L3.73553 17.0217L4.72103 13.7165L5.63453 17.0217L9.00953 18.0072L5.63453 19.0625ZM38.399 16.6032L39.4543 19.9085L42.6898 20.894L39.4543 21.9492L38.399 25.1847L37.3438 21.9492L34.1083 20.894L37.3438 19.9085L38.399 16.6032Z" fill="white" />
                        </svg>
                        <h2 className='text-5xl leading-[4rem] font-normal'>Remove Backgrounds Automatically</h2>
                    </div>
                    <p className='w-[40%] text-center text-lg leading-7 font-normal text-[#C8C8C8]'>Remove backgrounds for free and replace it with different backgrounds of your choosing.</p>
                </div>
                <div className='flex gap-20 py-1 justify-center'>
                    {/* Left */}
                    <div className='w-[15%] flex -rotate-6'>
                        <img className={`w-[80%] object-cover rounded-3xl`} src={bgRemoverImg} alt="bgRemoverImg" />
                        <svg className='animate-bgbar' width="70" height="350" viewBox="0 0 99 798" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="46" y="1" width="6" height="796" rx="3" fill="white" stroke="black" stroke-width="2" />
                            <circle cx="49.5854" cy="161.004" r="36.2639" fill="white" stroke="black" stroke-width="1.48016" />
                            <path d="M61.275 179.639C60.6454 179.64 60.0352 179.421 59.5504 179.019C59.2776 178.793 59.052 178.515 58.8867 178.201C58.7214 177.888 58.6195 177.545 58.587 177.192C58.5544 176.839 58.5918 176.483 58.6971 176.145C58.8023 175.806 58.9732 175.492 59.2001 175.219L71.2721 160.776L59.6313 146.306C59.4074 146.03 59.2403 145.713 59.1394 145.373C59.0385 145.032 59.0059 144.675 59.0435 144.322C59.0811 143.969 59.188 143.627 59.3583 143.315C59.5285 143.004 59.7586 142.729 60.0354 142.506C60.3143 142.261 60.6408 142.076 60.9945 141.963C61.3482 141.85 61.7214 141.811 62.0909 141.849C62.4603 141.887 62.8179 142.001 63.1412 142.183C63.4645 142.366 63.7465 142.614 63.9696 142.911L76.9848 159.078C77.3811 159.561 77.5978 160.165 77.5978 160.79C77.5978 161.414 77.3811 162.018 76.9848 162.501L63.5115 178.669C63.2412 178.995 62.8978 179.252 62.5092 179.421C62.1206 179.59 61.6978 179.664 61.275 179.639Z" fill="#231F20" />
                            <path d="M37.725 141.914C38.3546 141.913 38.9648 142.132 39.4496 142.534C39.7224 142.76 39.948 143.038 40.1133 143.351C40.2786 143.665 40.3805 144.008 40.413 144.361C40.4456 144.714 40.4081 145.069 40.3029 145.408C40.1977 145.746 40.0268 146.061 39.7999 146.333L27.7278 160.776L39.3687 175.247C39.5926 175.522 39.7597 175.839 39.8606 176.18C39.9615 176.52 39.994 176.877 39.9565 177.23C39.9189 177.584 39.812 177.926 39.6417 178.237C39.4715 178.549 39.2414 178.824 38.9645 179.046C38.6857 179.291 38.3592 179.477 38.0055 179.59C37.6518 179.703 37.2785 179.742 36.9091 179.704C36.5397 179.666 36.1821 179.552 35.8588 179.369C35.5355 179.186 35.2534 178.939 35.0303 178.642L22.0152 162.474C21.6188 161.992 21.4022 161.387 21.4022 160.763C21.4022 160.139 21.6188 159.534 22.0152 159.052L35.4884 142.884C35.7588 142.558 36.1021 142.3 36.4907 142.132C36.8793 141.963 37.3022 141.888 37.725 141.914Z" fill="#231F20" />
                        </svg>

                    </div>
                    <div className='w-[30%]'>
                        <Uploader />
                    </div>

                    {/* Right */}
                    <div className='w-[15%] flex flex-col justify-between items-center'>
                        <img className='w-[65%]' src={bgRemoverImg3} alt="bgImg3" />
                        <div className='flex flex-col gap-3 items-center'>
                            <p className='text-center text-[#C8C8C8] text-[0.85rem] leading-5 font-semibold'>No Image?<br />Try one of these:</p>
                            <img src={bgRemoverImg4} alt="bgImg4" />
                        </div>
                    </div>
                </div>
                <div className='flex justify-center py-6'>
                    <p className='w-[40%] text-center text-white'>By uploading an image, you agree to our Terms of Service. For details on how ZAF manages your personal data,
                        please refer to our Privacy Policy.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default BackgroundRemover