import resumescreen from './assets/carouselImages/resumescreen.png';
import drowsiness from './assets/carouselImages/drowsiness.png';
import background from './assets/carouselImages/background.png';
import emotionDetect from './assets/carouselImages/emotionDetect.png';

import hospitality from './assets/carouselImages/hospitality.png';
import logistics from './assets/carouselImages/logistics.png';
import airlines from './assets/carouselImages/airlines.png';
import realEstate from './assets/carouselImages/realEstate.png';


// Models
export const models = [
  {
    id: 1,
    name: 'Hospitality',
    desc: 'AI tools in hospitality enhance guest experiences, automate bookings, optimize operations, and personalize services.',
    img: hospitality,
    route: '/hospitality'
  },
  {
    id: 2,
    name: 'Logistics',
    desc: 'AI tools in logistics optimize operations, automate tracking, enhance efficiency, and improve delivery accuracy and customer satisfaction.',
    img: logistics,
    route: '/logistics'
  },
  {
    id: 3,
    name: 'Airlines',
    desc: 'AI tools in airlines enhance efficiency, automate bookings, optimize routes, improve safety, and personalize passenger experiences.',
    img: airlines,
    route: '/airlines'
  },
  {
    id: 4,
    name: 'Real Estate',
    desc: 'AI tools in real estate streamline property management, automate valuations, enhance marketing, and personalize buyer experiences.',
    img: realEstate,
    route: '/real-estate'
  },
  {
    id: 5,
    name: 'Finance',
    desc: 'Description Model 5',
    img: hospitality,
    route: '/finance'
  },
  {
    id: 6,
    name: 'Education',
    desc: 'Description Model 6',
    img: hospitality,
    route: '/education'
  },
  {
    id: 7,
    name: 'Healthcare',
    desc: 'Description Model 7',
    img: hospitality,
    route: '/healthcare'
  },
  {
    id: 8,
    name: 'Model 8',
    desc: 'Description Model 8',
    img: hospitality,
    route: '/model8'
  },
  {
    id: 9,
    name: 'Model 9',
    desc: 'Description Model 9',
    img: hospitality,
    route: '/model9'
  },
  {
    id: 10,
    name: 'Model 10',
    desc: 'Description Model 10',
    img: hospitality,
    route: '/model10'
  },
  {
    id: 11,
    name: 'Model 11',
    desc: 'Description Model 11',
    img: hospitality,
    route: '/model11'
  },
  {
    id: 12,
    name: 'Model 12',
    desc: 'Description Model 12',
    img: hospitality,
    route: '/model12'
  },
  {
    id: 13,
    name: 'Model 13',
    desc: 'Description Model 13',
    img: hospitality,
    route: '/model13'
  },
  {
    id: 14,
    name: 'Model 14',
    desc: 'Description Model 14',
    img: hospitality,
    route: '/model14'
  },
  {
    id: 15,
    name: 'Model 15',
    desc: 'Description Model 15',
    img: hospitality,
    route: '/model15'
  }
];

// Trending AI Tools
export const tools = [
  {
    id: 1,
    name: 'Resume Screening',
    desc: ' Accelerate your hiring process by automatically screening resumes to find the best candidates quickly and efficiently.',
    img: resumescreen,
    route: '/resume-screening'
  },
  {
    id: 2,
    name: 'Drowsiness Detection',
    desc: 'Enhance safety with real-time drowsiness detection, crucial for transportation and workplace safety.',
    img: drowsiness,
    route: '/drowsiness-detection'
  },
  {
    id: 3,
    name: 'Background Remover',
    desc: 'Instantly remove backgrounds from images with precision, saving time and effort for designers and marketers.',
    img: background,
    route: '/background-remover'
  },
  {
    id: 4,
    name: 'Emotion Detection',
    desc: 'Understand customer sentiment and improve service with our powerful  emotion detection tool, perfect for marketing  analysis.',
    img: emotionDetect,
    route: '/emotion-detection'
  },
  {
    id: 5,
    name: 'Unattended Baggage',
    desc: 'Unattended Baggage Description',
    img: resumescreen,
    route: '/unattended-baggage'
  },
  {
    id: 6,
    name: 'Tool 6',
    desc: 'Tool 6 Description',
    img: resumescreen,
    route: '/tool6'
  },
  {
    id: 7,
    name: 'Tool 7',
    desc: 'Tool 7 Description',
    img: resumescreen,
    route: '/tool7'
  }
];

// Members 
export const members = [
  {
    id: 1,
    name: 'Member 1',
    route: '/member1'
  },
  {
    id: 2,
    name: 'Member 2',
    route: '/member2'
  },
  {
    id: 3,
    name: 'Member 3',
    route: '/member3'
  },
  {
    id: 4,
    name: 'Member 4',
    route: '/member4'
  },
  {
    id: 5,
    name: 'Member 5',
    route: '/member5'
  },
  {
    id: 6,
    name: 'Member 6',
    route: '/member6'
  },
  {
    id: 7,
    name: 'Member 7',
    route: '/member7'
  },
  {
    id: 8,
    name: 'Member 8',
    route: '/member8'
  },
  {
    id: 9,
    name: 'Member 9',
    route: '/member9'
  },
  {
    id: 10,
    name: 'Member 10',
    route: '/member10'
  }
]