import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './pages/Home';
import './App.css';
import BackgroundRemover from "./pages/BackgroundRemover";
import BgRemovedResult from "./pages/BgRemovedResult";
import ScrollToTop from "./components/ScrollToTop";

function App() {
  const paths = [
    { path: "/", element: <Home /> },
    { path: "/background-remover", element: <BackgroundRemover /> },
    { path: "/bg-result", element: <BgRemovedResult /> }
  ]
  return (
    <div className="app">
      <BrowserRouter>
        <ScrollToTop>
          <Routes>
            {paths.map((path, index) => (
              <Route key={index} path={path.path} element={path.element} />
            ))}
          </Routes>
        </ScrollToTop>
      </BrowserRouter>
    </div>
  );
}

export default App;
