/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    fontFamily: {
      outfit: ['"Outfit", sans-serif']
    },
    extend: {
      keyframes: {
        bgbar: {
          '0%': { transform: 'translateX(0rem)' },
          '40%': { transform: 'translateX(-7rem)' },
        }
      },
      animation: {
        bgbar: 'bgbar 10s ease-in-out infinite',
      }
    },
  },
  plugins: [],
}
