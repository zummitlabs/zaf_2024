# **Gender Detection and Age Prediction**

This project leverages OpenCV's Deep Neural Network (DNN) module to perform **real-time face detection** from webcam input, followed by predicting the **gender** and **age group** of each detected face. It uses pre-trained models for accuracy and efficiency.

---

## **Project Overview**

The program processes a video stream from a webcam, identifies human faces in the frame, and uses machine learning models to:
- Classify the **gender** of the detected face as Male or Female.
- Estimate the **age group** from a set of predefined categories.

The output is displayed on the live video feed, with a bounding box around each face and labels showing the predicted gender and age.

---

## **How It Works**

### **1. Face Detection**
The program uses a pre-trained face detection model (`opencv_face_detector`) to locate faces in the video frames. It applies a bounding box around each detected face.

### **2. Age Prediction**
The **age detection model** predicts the age group of the detected face. The model provides probabilities for each age group, and the one with the highest probability is chosen.

Age groups are predefined as:
- `(0-2)`
- `(4-6)`
- `(8-12)`
- `(15-20)`
- `(25-32)`
- `(38-43)`
- `(48-53)`
- `(60-100)`

### **3. Gender Prediction**
The **gender detection model** predicts whether the detected face is **Male** or **Female**.

Both models are based on pre-trained DNN architectures, ensuring efficient and accurate predictions.

---

## **Key Features**
- Real-time face detection and labeling.
- Pre-trained models for both gender and age prediction.
- Displays gender and age group predictions directly on the video feed.
- Simple to set up and run with a standard webcam.

---

## **Requirements**

### **Software**
- Python 3.x
- OpenCV library (for DNN and webcam integration)

### **Pre-trained Models**
- `opencv_face_detector.pbtxt` and `opencv_face_detector_uint8.pb`: Face detection model.
- `age_net.caffemodel` and `age_deploy.prototxt`: Age prediction model.
- `gender_net.caffemodel` and `gender_deploy.prototxt`: Gender prediction model.

---

## **Installation and Setup**

### **Step 1: Clone the Repository**
Download or clone the project repository to your local machine:
```bash
git clone <repository-URL>
cd <project-folder>
```

### **Step 2: Install Dependencies**
Install the required Python libraries using `pip`:
```bash
pip install opencv-python
```

### **Step 3: Place Pre-trained Models**
Ensure the following files are in the same directory as the Python script:
- `opencv_face_detector.pbtxt`
- `opencv_face_detector_uint8.pb`
- `age_net.caffemodel`
- `age_deploy.prototxt`
- `gender_net.caffemodel`
- `gender_deploy.prototxt`

---

## **Usage**

1. Open a terminal and navigate to the project directory.
2. Run the Python script:
   ```bash
   python gender_age_detection.py
   ```
3. The program will activate your webcam and display the live video feed with the following:
   - Bounding boxes around detected faces.
   - Labels showing the predicted **gender** and **age group**.

4. To stop the program, press the `q` key.

---

## **Expected Output**
The program displays the live webcam feed with the following annotations for each detected face:
- **Bounding Box**: A rectangle around the detected face.
- **Label**: Text above the bounding box showing gender and age group in the format:
  ```
  Male, (25-32)
  ```

### **Example Labels**
- Male, (15-20)
- Female, (38-43)

---

## **File Descriptions**
- **`gender_age_detection.py`**: Main Python script that performs face detection, gender prediction, and age prediction.
- **Pre-trained Model Files**:
  - `opencv_face_detector.pbtxt`: Face detection model configuration.
  - `opencv_face_detector_uint8.pb`: Face detection model weights.
  - `age_net.caffemodel`: Age prediction model weights.
  - `age_deploy.prototxt`: Age prediction model configuration.
  - `gender_net.caffemodel`: Gender prediction model weights.
  - `gender_deploy.prototxt`: Gender prediction model configuration.

---

## **Customizations**
You can modify the following parameters to customize the program:
1. **Confidence Threshold**: The minimum confidence for detecting a face (currently set to `0.7`).
2. **Padding**: Adjust the `padding` value to increase or decrease the area around detected faces for age/gender prediction.
3. **Webcam Source**: Change the video capture source (`cv2.VideoCapture(0)`) to use a different camera or video file.

---

## **Notes**
- Ensure your webcam is connected and working.
- The accuracy of predictions depends on the quality of the pre-trained models.
- Models are optimized for frontal faces in good lighting conditions.

---

## **Acknowledgments**
- **OpenCV DNN Module**: Used for real-time face detection.
- Pre-trained models for gender and age prediction were sourced from publicly available repositories.

---

